package com.renseki.app.totandroid.ui.aboutme

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import com.renseki.app.totandroid.model.Me
import com.renseki.app.totandroid.repository.MeRepository

class AboutMeViewModel(
    private val meRepository: MeRepository
) : ViewModel() {

    private val initialMe = meRepository.load()

    val modifiedMe = MediatorLiveData<Me>().apply {
        addSource(initialMe) { me ->
            value = me
        }
    }

    private val persistMeSignal = MutableLiveData<Boolean>()

    /* Alternative 1 */

//    var persistedMe = MediatorLiveData<Me>().apply {
//        addSource(persistMeSignal) { persistNow ->
//            if (persistNow == true) {
//                modifiedMe.value?.also { me ->
//                    val persistedMe = meRepository.persist(me)
//                    addSource(persistedMe) {
//                        value = it
//                    }
//                }
//            }
//        }
//    }

    /* Alternative 2 */

    var persistedMe = Transformations.switchMap(persistMeSignal) { persistNow ->
        if (persistNow == true) {
            modifiedMe.value?.let { me ->
                meRepository.persist(me)
            } ?: MutableLiveData()
        } else {
            MutableLiveData()
        }
    }

    fun save() {
        persistMeSignal.value = true
    }
}