package com.renseki.app.totandroid.ui.aboutme

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.renseki.app.totandroid.R
import com.renseki.app.totandroid.commons.SingleInputActivity
import com.renseki.app.totandroid.databinding.AboutMeActivityBinding
import com.renseki.app.totandroid.repository.MeRepository
import com.renseki.app.totandroid.service.MeLocalService
import com.renseki.app.totandroid.util.extension.toast

class AboutMeActivity : AppCompatActivity() {

    companion object {
        private const val SINGLE_INPUT_CODE = 888

        fun getStartIntent(context: Context)
                = Intent(context, AboutMeActivity::class.java)
    }

    private lateinit var binding: AboutMeActivityBinding
    private lateinit var viewModel: AboutMeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(
            this, R.layout.about_me_activity
        )

        viewModel = getViewModel()

        binding.tvLocation.text = "Ngagel Jaya Tengah 73-77"
        binding.viewModel = viewModel

        viewModel.persistedMe?.observe(this, Observer {
            it?.also { me ->
                toast(getString(R.string._has_been_persisted, me.name))
            }
        })

        binding.tvPhone.setOnClickListener {
            viewModel.modifiedMe.value?.also { me ->
                val dialIntent = Intent(Intent.ACTION_DIAL)
                dialIntent.data = Uri.parse("tel:${me.phone}")
                startActivity(dialIntent)
            }
        }

        binding.tvLocation.setOnClickListener {
            val locationIntent = Intent(Intent.ACTION_VIEW)
            locationIntent.data = Uri.parse("geo:-7.291306,112.7566403")
            startActivity(locationIntent)
        }

        binding.tvName.setOnClickListener {
            viewModel.modifiedMe.value?.also { me ->
                startActivityForResult(
                    SingleInputActivity.getStartIntent(
                        this,
                        me.name
                    ),
                    SINGLE_INPUT_CODE
                )
            }
        }
    }

    override fun onStart() {
        super.onStart()
        binding.setLifecycleOwner(this)
    }

    override fun onStop() {
        binding.setLifecycleOwner(null)
        super.onStop()
    }

    private fun getViewModel(): AboutMeViewModel {
        val viewModelFactory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel?> create(modelClass: Class<T>): T {
                return AboutMeViewModel(
                    MeRepository(
                        MeLocalService(
                            this@AboutMeActivity
                        )
                    )
                ) as T
            }
        }

        return ViewModelProviders
            .of(this, viewModelFactory)
            .get(AboutMeViewModel::class.java)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode) {
            SINGLE_INPUT_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val newValue = data?.getStringExtra(
                        SingleInputActivity.DATA_VALUE
                    ) ?: ""

                    val oldMe = viewModel.modifiedMe.value
                    viewModel.modifiedMe.value = oldMe?.copy(
                        name = newValue
                    )

                    viewModel.save()
                }
            }
            else -> {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }
    }
}
