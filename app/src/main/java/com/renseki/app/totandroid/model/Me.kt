package com.renseki.app.totandroid.model

data class Me(
    val name: String,
    val imageUrl: String,
    val email: String,
    val phone: String
)