package com.renseki.app.totandroid.binding

import android.databinding.BindingAdapter
import android.widget.ImageView
import com.bumptech.glide.Glide

object ImageViewDataBindingAdapter {
    @JvmStatic
    @BindingAdapter("imageUrl")
    fun setImageUrl(imageView: ImageView, imageUrl: String) {
        Glide
            .with(imageView.context)
            .load(imageUrl)
            .into(imageView)
    }
}