package com.renseki.app.totandroid.service

import android.content.Context
import android.content.SharedPreferences
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import org.junit.Test

class MeLocalServiceTest {

    private val context: Context = mock()
    private val mockedSharedPref: SharedPreferences = mock()

    @Test
    fun `load me, success, and get value`() {
        val service = MeLocalService(context)
        val result = service.load()

        whenever(context.getSharedPreferences("settings", 0))
            .thenReturn(mockedSharedPref)

        //todo: mock getString

        result.observeForever(mock())

        val me = result.value
        assert(me != null)
        assert(me!!.name == "Dummy Name")
    }
}